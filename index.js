require('dotenv').config() // kegunaan untuk membaca file .env
const express = require('express')
const port = process.env.PORT || 3500 // declare port dari env 
const app = express() // inisiasi function express ke varable app
const cors = require('cors') // inisiasi variabel bersi cors
const bodyParser = require('body-parser')
const router = require('./router')
const path = require('path')

app.use(express.static(path.join(__dirname, 'public')))
app.set('view engine', 'hbs');
app.use(cors());
app.use(express.urlencoded({extended : true}))
app.use(bodyParser.json())
app.use('/', router)


app.listen(port, () => {
    console.log(`server is running on port ${port}`);
})