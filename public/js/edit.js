$('button[type="submit"]').on('click', function(e){
    e.preventDefault();

    const id = $(this).attr('data-id');

    var name = $("#inputnama").val();
    var price = $("#inputprice").val();
    var fPrice = parseFloat(price);
    var size = $("#size").val();
    var file = $("#inputimg").val();
    const datacar = {
        car_name : name,
        price : fPrice,
        size : size,
        image : file
    }

    $.ajax({
        url: '/car/update/' + id,
        method : 'PUT',
        data : datacar,
        success: function () {
            $(location).attr('href', '/car/carlist');
            var alertPlaceholder = document.getElementById('alert')
            function alert(message, type) {

                var wrapper = document.createElement('div')
                wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert" style="position:absolute; width:100%; left: 50%; transform: translate(-50%, 0);">' + message + '</div>'
                alertPlaceholder.append(wrapper)
            }
            alert('Data Berhasil Diupdate', 'success');
        }

    })
    // .then(function(){
    //     $(location).attr('href', '/car/carlist');
    //     var alertPlaceholder = Document.getElementByID('alert')
    //     function alert(message, type) {
    
    //         var wrapper = document.createElement('div')
    //         wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert" style="position:absolute; left: 50%; transform: translate(-50%, 0);">'  + message + '</div>'
    //         alertPlaceholder.append(wrapper)
    //       }
    //     alert('Data Berhasil Disimpan', 'success');
    // })
    
});

