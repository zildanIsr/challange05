$('.carlist .btn-grup .del-btn button').on('click', function () {
    var id = $(this).attr('data-id');
    
    var alertdel = $("#pop-up")
    $('<div/>',{ // <------------------create a dynamic overlay div
        id:"bg-dark"
    }).appendTo("body"); //<-----append to body.
    var popmessage = document.createElement('div');
    popmessage.innerHTML = `<div id="overlay" class=" bg-light px-5 py-4 rounded h-100">
    <div class="d-flex flex-column justify-contenct-center">
        <div class="image mx-auto">
            <img src="/assets/img-BeepBeep.png" alt="" srcset="">
        </div>
        <div class="text-center">
            <p class="fw-bold fs-5">Menghapus Data Mobil</p>
            <p>Setelah dihapus, data mobil tidak dapat dikembalikan. Yakin ingin menghapus?</p>
        </div>
        <div class="button mx-auto">
            <button class="btn btn-primary px-4 py-2 me-3" data-id="${id}" id="yes-btn" onclick="deletecar();">Ya</button>
            <button class="btn btn-outline-primary px-3 py-2" onclick="exit();" id="cancel-btn">Tidak</button>
        </div>
        </div>
    </div>`;
    alertdel.append(popmessage)
    
});

function deletecar(){
    var id = $('#yes-btn').attr('data-id');
    $.ajax({
        url: '/car/delete/' + id,
        method : 'DELETE',
        success: function () {
            $(location).attr('href', '/car/carlist');
            var alertPlaceholder = document.getElementById('alert')
            function alert(message, type) {

                var wrapper = document.createElement('div')
                wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert" style="position:absolute; width:100%; left: 50%; transform: translate(-50%, 0);">' + message + '</div>'
                alertPlaceholder.append(wrapper)
            }
            alert('Data Berhasil Dihapus', 'success');
        }

    })
}

function exit(){
    $('#bg-dark').remove();
    $('#overlay').remove();
}

$('.carlist .btn-grup .editbtn button').on('click', function () {
    var id = $(this).attr('data-id');
    $(location).attr('href', '/car/edit/' + id)
    
});

