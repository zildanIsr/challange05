const express = require('express')
const { list, create, deleteone, update, listcar, createcar, updateone } = require('../controllers/carController')
const router = express.Router()
const validate = require('../middlewares/validate')
const { createCarCategoryRules } = require('../validators/carRules')


router.get('/cars', (req, res) => {
    res.render('cars', {
      subject: 'Rent Car Binar'
    }); 
});

router.get('/carlist', listcar);
router.get('/createcar', (req, res) => {
    res.render('create', {
        subject: 'Rent Car Binar',
        endpoint: 'Add New Car'
    });
});
router.get('/edit/:id', updateone)

router.post('/newcreate', validate(createCarCategoryRules), createcar)

//POSTMAN
router.get('/list', list)
router.post('/create', validate(createCarCategoryRules), create)
router.put('/update/:id', update)
router.delete('/delete/:id', deleteone)

module.exports = router