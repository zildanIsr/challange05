const express = require('express')
const carRouter = require('./carRouter')
const app = express.Router()


app.get('/', (req, res) => {
    res.render('index', {
      subject: 'Rent Car Binar'
    });
});

app.get('/check-status', (req, res) => {
    res.send('ok')
})

app.use('/car', carRouter)

module.exports = app