# Challange 05
## Express, Node JS, Postgres SQL, and View Engine
## Installation

Dillinger requires [Node.js](https://nodejs.org/) v10+ to run.

Install the dependencies and devDependencies and start the server.

```sh
npm install
or
yarn install
```


## ERD

Entity relationship diagram dar tabel car

| Key | cars |
| --- | ------ |
| pk | id |
|  | car_name varchar(25) NOT NULL |
|  | price float NOT NULL |
|  | size varchar(10) NOT NULL |
|  | image varchar(50) NOT NULL |
|  | createdAt timestamp |
|  | updateAt timestamp |


## Features

CRUD aplication with express - sequelize

server running on
```sh
localhost:3000
```

Router for check connection
```sh
/                   --> default render index.hbs
/check-status       --> response send 'ok'
```

### Router CRUD with POSTMAND

app.use("/car", carRouter)

```sh
car/list            --> 'GET'  Method for find all datas from database
car/create          --> 'POST' Method for create a new data/car to database
car/update/:id      --> 'PUT' Method for update a data from db with spesific id
car/delete/:id      --> 'DELETE' Method for delete a data from db with spesific id
```

request body example for car/create :
```
{
    car_name : "Avanza",
    price : 15000,
    size : "Medium",
    image : "https://picsum.photos/200/300/?random"
}
```

response body example :
```
res status (200)
{
    'success' : true,
    'error' : 0,
    'message' : 'Data Successfully created',
    'data' : {
                car_name : "Avanza",
                price : 15000,
                size : "Medium",
                image : "https://picsum.photos/200/300/?random"
            }
}

res status (500)
{
    'success' : false,
    'error' : error.code,
    'message' : error,
    'data' : null
}
```

### Router CRUD with View Engine

app.use("/car", carRouter)

```sh
car/carlist         --> 'GET'  Method for find all datas from database
```
return response render list.hbs 
```sh
const datas = await model.car.findAll()
return res.render('list', {
        card_cars: datas,
        subject: 'Rent Car Binar',
        endpoint: 'List Cars'
}); 
```
--
```sh
car/createcar         --> 'GET'  
```
return response render create.hbs 
```sh
res.render('create', {
        subject: 'Rent Car Binar',
        endpoint: 'Add New Car'
    });
}); 
```
--
```sh
car/newcreate           --> 'POST'  
```
return response status (200) if succes and create new data
--
```sh
car/edit/:id           --> 'PUT'  
```
return response render edit.hbs 
```sh
return res.render('edit', {
    subject: 'Rent Car Binar',
    endpoint: 'Edit Car',
    id: id,
});
```
