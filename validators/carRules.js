const { body } = require('express-validator')

const createCarCategoryRules = [
    body('car_name').notEmpty().withMessage('name is required'),
    body('price').notEmpty().withMessage('price is required'),
    body('size').notEmpty().withMessage('size is required'),
]

module.exports = {
    createCarCategoryRules
}