const model = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const datas = await model.car.findAll()

            return res.status(200).json({
                'success' : true,
                'error' : 0,
                'message' : 'Data Successfully list',
                'data' : datas
            })
        } catch (error) {
            return res.status(500).json({
                'success' : false,
                'error' : error.code,
                'message' : error,
                'data' : null
            })
        }
    }, 
    listcar: async (req, res) => {
        try {
            const datas = await model.car.findAll()
            return res.render('list', {
                card_cars: datas,
                subject: 'Rent Car Binar',
                endpoint: 'List Cars'
                    
            }); 
        } catch (error) {
            return res.status(500).json({
                'success' : false,
                'error' : error.code,
                'message' : error,
                'data' : null
            })
        }
    },
    createcar: async (req, res) => {
        try {
            const { car_name, price, size, image } = req.body
            const datas = await model.car.create({
                car_name : car_name,
                price : price,
                size : size,
                image : image
            })

            return res.status(200).json({
                'success' : true,
                'error' : 0,
                'message' : 'Data Successfully created',
                'data' : datas
            })
        } catch (error) {
            return res.status(500).json({
                'success' : false,
                'error' : error.code,
                'message' : error,
                'data' : null
            })
        }
    },
    create: async (req, res) => {
        try {
            const { car_name, price, size, image } = req.body
            const datas = await model.car.create({
                car_name,
                price,
                size,
                image
            })

            return res.status(200).json({
                'success' : true,
                'error' : 0,
                'message' : 'Data Successfully created',
                'data' : datas
            })
        } catch (error) {
            return res.status(500).json({
                'success' : false,
                'error' : error.code,
                'message' : error,
                'data' : null
            })
        }
    },
    update: async (req, res) => {
        try {
            const  id  = req.params.id
            const { car_name, price, size, image } = req.body
            const datas = await model.car.update({
                car_name,
                price,
                size,
                image
            }, {
                where: {
                    id: id
                }
            })

            return res.status(200).json({
                'success' : true,
                'error' : 0,
                'message' : 'Data Successfully updated',
                'data' : datas
            })
        } catch (error) {
            return res.status(500).json({
                'success' : false,
                'error' : error.code,
                'message' : error,
                'data' : null
            })
        }
    },
    updateone: async (req, res) => {
        try {
            const  id  = req.params.id
            const { car_name, price, size, image } = req.body
            const datas = await model.car.update({
                car_name: car_name,
                price: price,
                size: size,
                image: image
            }, {
                where: {
                    id: id
                }
            })

            return res.render('edit', {
                subject: 'Rent Car Binar',
                endpoint: 'Edit Car',
                id: id,
            });
        } catch (error) {
            return res.status(500).json({
                'success' : false,
                'error' : error.code,
                'message' : error,
                'data' : null
            })
        }
    },
    deleteone: async (req, res) => {
        try {
            const id = req.params.id
            const datas = await model.car.destroy({
                where: {
                    id : id
                }
            })

            return res.status(200).json({
                'success' : true,
                'error' : 0,
                'message' : 'Data Successfully deleted',
                'data' : datas
            })
        } catch (error) {
            return res.status(500).json({
                'success' : false,
                'error' : error.code,
                'message' : error,
                'data' : null
            })
        }
    }
}